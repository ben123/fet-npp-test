<html lang="en">
<%@include file="/WEB-INF/header.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Edit Environment</title>
<style>
    .error {
        color: #FF0000;
    }
</style>

</head>
<body>
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="list-group">
                    <a href="npp/history" class="list-group-item">History</a>
                    <a href="npp/echo" class="list-group-item">Echo Testing</a> 
                    <a href="npp/manualTesting" class="list-group-item">Manual Testing</a>
                    <a href="npp/check" class="list-group-item">Check Request File</a>
                    <a href="npp/editEnvironment" class="list-group-item active">Edit Environment</a>
                </div>
            </div>

            <div class="col-md-9">
                <div class="thumbnail">
                    <div class="caption-full">
                        <form class="form-horizontal" role="form" id="editEnvironmentForm" method="post" action="npp/saveEnvironmentConfig">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="id_display_name">Display name:</label>
                                <div class="col-md-7">
                                        <input type="text" name="display_name" maxLength="250" value="${display_name}" id="id_display_name" class="form-control" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="dcbTosVersion">DCB ToS Version:</label>
                                <div class="col-md-7">
                                        <input type="text" name="dcbTosVersion" maxLength="250" value="${dcbTosVersion}" id="dcbTosVersion" class="form-control" />
                                </div>
                            </div>
                            
                            <div class="form-group">
                               	<label class="col-md-3 control-label" for="total">Total $:</label>
                               	<div class="col-md-7">
                                    <input type="text" id="total" size="12" maxLength="12" name="total" class="numeric form-control" value="${total}" />
                               	</div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-7">
                                    <input type="submit" id="saveEnviBtn" class="btn btn-primary" name="save" value="Save Environment" data-loading-text="Saveing...">
                                </div>
                            </div>
                            
                            <div class="form-group">
	                            <div class="col-md-offset-3 col-md-7">
	                                <input type="button" id="runRequestFile" class="btn btn-primary" name="runRequestFile" value="Run Request File" data-loading-text="Running...">
	                            </div>
	                        </div>
	                        <div class="form-group">
                               	<label class="col-md-3 control-label" for="total">Last Batch Time:</label>
                               	<div class="col-md-7">
                                    <input type="text" id="lastBatchTime" name="lastBatchTime" class="form-control" value="${lastBatchTime}" readonly />
                               	</div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
    $(document).ready(function() {
    	$('#saveEnviBtn').on('click', function(){
    		$(this).button('loading');
    	});
    	$('#runRequestFile').on('click', function(){
    		$(this).button('loading');
    		var projectName = window.location.pathname.split('/')[1];
        	window.location.href = 'http://'+window.location.host+'/'+projectName+'/'+'npp/runRequestFile';
    	});
    	$(".numeric").numeric();
    });
</script>
</html>
