<!DOCTYPE html>
<html lang="en">
    <head>
        <%@include file="/WEB-INF/header.jsp" %>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Request File Details</title>
    </head>
    <body>
        <!-- Page Content -->
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="list-group">
                        <a href="npp/history" class="list-group-item">History</a>
                        <a href="npp/echo" class="list-group-item">Echo Testing</a>
                        <a href="npp/manualTesting" class="list-group-item">Manual Testing</a>
                        <a href="npp/check" class="list-group-item active" >Check Request File</a>
                        <a href="npp/editEnvironment" class="list-group-item">Edit Environment</a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="thumbnail">
                        <div class="caption-full">
                            <div class="table-responsive">
                                <div>
                                    <center>Request File Details</center>
                                    <table class="table table-bordered">
                                        <tr class="active">
                                            <th>Correlation Id</th>
                                            <th>Request Type</th>
                                            <th>NPP Status</th>
                                        </tr>
                                        <c:forEach var="requestFileDetail" begin="0" items="${requestScope.requestFileDetail}">
                                        <tr>
                                            <td>${requestFileDetail.correlationId}</td>
                                            <td>${requestFileDetail.requestType}</td>
                                            <td>${requestFileDetail.status}</td>
                                        </tr>
                                        </c:forEach>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>