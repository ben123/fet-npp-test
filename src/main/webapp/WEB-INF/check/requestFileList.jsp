<!DOCTYPE html>
<html lang="en">
    <head>
        <%@include file="/WEB-INF/header.jsp"%>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Request File List</title>
    </head>
    <body>
        <!-- Page Content -->
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="list-group">
                        <a href="npp/history" class="list-group-item">History</a> 
                        <a href="npp/echo" class="list-group-item">Echo Testing</a>
                        <a href="npp/manualTesting" class="list-group-item">Manual Testing</a> 
                        <a href="npp/check" class="list-group-item active">Check Request File</a>
                        <a href="npp/editEnvironment" class="list-group-item">Edit Environment</a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="thumbnail">
                        <div class="caption-full">
                            <form id="requestFileListSearchForm" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="out">Operator user token:</label>
                                    <div class="input-group col-md-7">
                                        <input type="text" id="out" name="out" maxLength="255" value="<c:if test="${'0' != out}">${out}</c:if>" class="form-control" />
                                        <span></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="correlationIdFrom">Correlation id from:</label>
	                               	<div class="input-group col-md-7">
	                               		<input type="text" id="correlationIdFrom" maxLength="17" name="correlationIdFrom" class="numeric form-control" value="<c:if test="${'0' != correlationIdFrom}">${correlationIdFrom}</c:if>"/>
	                               		<span class="input-group-addon">to</span>
	                               		<input type="text" id="correlationIdTo" maxLength="17" name="correlationIdTo" class="numeric form-control" value="<c:if test="${'0' != correlationIdTo}">${correlationIdTo}</c:if>"/>
	                               	</div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group col-md-offset-3 col-md-7">
                                        <input type="button" class="btn btn-primary" name="search" value="search">
                                        <span></span>
                                    </div>
                                </div>
                            </form>
                            <nav>
                                <ul class="pagination">
                                    <li <c:if test="${disablePrevious}">class="disabled"</c:if>>
                                    <a <c:if test="${!disablePrevious}">href="npp/check/${out}/${correlationIdFrom}/${correlationIdTo}/${begin - 1}"</c:if> aria-label="Previous" id="pagePrevious">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                    </li>
                                    <c:forEach var="i" begin="${begin}" end="${end}" step="1">
                                        <li <c:if test="${i == clickPage}">class="active"</c:if>><a href="npp/check/${out}/${correlationIdFrom}/${correlationIdTo}/${i}">${i}</a></li>
                                    </c:forEach>
                                    <li <c:if test="${disableNext}">class="disabled"</c:if>>
                                        <a <c:if test="${!disableNext}">href="npp/check/${out}/${correlationIdFrom}/${correlationIdTo}/${end + 1}"</c:if> aria-label="Next" id="pageNext">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                            <table class="table table-striped">
                                <tr class="active">
                               <!-- <th>Operator User Token</th>
                                    <th>Correlation Id</th> -->
                                    <th>Creation Time</th>
                                    <th>File Name</th>
                                    <th>NPP Status</th>
                                    <!-- <th>Creation Time</th> -->
                                    <th>Detail Link</th>
                                </tr>
                                <c:forEach var="requestFileList" begin="0" items="${requestScope.requestFileList}">
                                    <tr style="word-break: break-all">
                                   <!-- <td>${requestFileList.operatorUserToken}</td>
                                        <td>${requestFileList.correlationId}</td> -->
                                        <td>${requestFileList.creationTime}</td>
                                        <td>${requestFileList.requestFileName}</td>
                                        <td>${requestFileList.status}</td>
                                        <!-- <td>${requestFileList.creationTime}</td> -->
                                        <td><c:if test="${not empty requestFileList.status}"><a href="npp/check/fileDetails/${requestFileList.fileDetailId}">File Detail</a></c:if></td>
                                    </tr>
                                </c:forEach>
                            </table>
                            <nav>
                                <ul class="pagination">
                                    <li <c:if test="${disablePrevious}">class="disabled"</c:if>>
                                        <a <c:if test="${!disablePrevious}">href="npp/check/${out}/${correlationIdFrom}/${correlationIdTo}/${begin - 1}"</c:if> aria-label="Previous" id="pagePrevious">
                                            <span aria-hidden="true">&laquo;</span>
                                        </a>
                                    </li>
                                    <c:forEach var="i" begin="${begin}" end="${end}" step="1">
                                        <li <c:if test="${i == clickPage}">class="active"</c:if>><a href="npp/check/${out}/${correlationIdFrom}/${correlationIdTo}/${i}">${i}</a></li>
                                    </c:forEach>
                                    <li <c:if test="${disableNext}">class="disabled"</c:if>>
                                        <a <c:if test="${!disableNext}">href="npp/check/${out}/${correlationIdFrom}/${correlationIdTo}/${end + 1}"</c:if> aria-label="Next" id="pageNext">
                                            <span aria-hidden="true">&raquo;</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
	    $(document).ready(function () {
	        $(".numeric").numeric();
	    });
        $('#requestFileListSearchForm input[name=search]').click(function () {
            var out = !$("#out").val() ? 0 : $("#out").val();
            var correlationIdFrom = !$("#correlationIdFrom").val() ? 0 : $("#correlationIdFrom").val();
            var correlationIdTo = !$("#correlationIdTo").val() ? 0 : $("#correlationIdTo").val();
            var projectName = window.location.pathname.split('/')[1];
            window.location.href = "http://"+window.location.host+"/"+projectName+"/npp/check/" + out + '/' + correlationIdFrom + '/' + correlationIdTo + '/1';
        });
    </script>
</html>