<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="/WEB-INF/header.jsp" %>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Manual Testing</title>
    <style>
        .error {
            color:#FF0000;
        }
        .placeholder{color: grey;}
		select option:first-child{color: grey; display: none;}
		select option{color: #555;} // bootstrap default color
    </style>
</head>
<body>
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="list-group">
                    <a href="npp/history" class="list-group-item">History</a> 
                    <a href="npp/echo" class="list-group-item">Echo Testing</a> 
                    <a href="npp/manualTesting" class="list-group-item active">Manual Testing</a>
                    <a href="npp/check" class="list-group-item">Check Request File</a>
                    <a href="npp/editEnvironment" class="list-group-item">Edit Environment</a>
                </div>
            </div>
            <div class="col-md-9">
                <div class="thumbnail">
                    <div class="caption-full">
                        <form class="form-horizontal" role="form" id="manualTestingForm">
                            <div class="form-group">
                               	<label class="col-md-3 control-label" for="out"><font size ="3"color="red">*</font>Operator user token:</label>
                               	<div class="col-md-7">
                                    <input type="text" id="out" name="out" size="255" maxLength="255" required class="form-control" />
                               	</div>
                            </div>
                            <div class="form-group">
                             	<label class="col-md-3 control-label" for="correlationId">Correlation id:</label>
                              	<div class="col-md-7">
                                    <input type="text" id="correlationId" size="20" maxLength="17" name="correlationId" class="numeric form-control"/>
                              	</div>
                            </div>
                            <div class="form-group">
	                            <label class="col-md-3 control-label">SOAP Calls:</label>
                              	<div class="col-md-7">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="provision" id="provision" value="Provision"> Provisioning Check
                                        </label>
                                        <label>
                                            <input type="checkbox" name="auth" id="auth" value="Auth"> Auth Check
                                        </label>
                                    </div>
                              	</div>
                            </div>
                            <div class="form-group" id="totalDiv" style="display: none">
                               	<label class="col-md-3 control-label" for="total"><font size ="3"color="red">*</font>Total $:</label>
                               	<div class="col-md-7">
                                    <input type="text" id="total" size="12" maxLength="12" name="total" required class="numeric form-control" value="${total}"/>
                               	</div>
                            </div>
                            <div class="form-group" id="dcbTosVersionDiv" style="display: none">
                               	<label class="col-md-3 control-label" for="dcbTosVersion"><font size ="3"color="red">*</font>DCB ToS Version:</label>
                               	<div class="col-md-7">
                                    <input type="text" id="dcbTosVersion" size="5" maxLength="5" name="dcbTosVersion" required class="form-control" value="${dcbTosVersion}"/>
                               	</div>
                            </div>
                            <div class="form-group" id="subscriptionIdDiv" style="display: none">
                               	<label class="col-md-3 control-label" for="subscriptionId">Subscription Id:</label>
                               	<div class="col-md-7">
                                    <input type="text" id="subscriptionId" name="subscriptionId" class="form-control" />
                               	</div>
                            </div>
                            <div class="form-group" id="subscriptionRecurrenceDiv" style="display: none">
                               	<label class="col-md-3 control-label" for="subscriptionRecurrence">Subscription Recurrence:</label>
                               	<div class="col-md-7">
	                                <select class="form-control placeholder" id="subscriptionRecurrence" name="subscriptionRecurrence">
                                        <option value="">per Transaction</option>
                                        <option value="DAILY">DAILY</option>
                                        <option value="MONTHLY">MONTHLY</option>
                                        <option value="YEARLY">YEARLY</option>
                                    </select>
                               	</div>
                            </div>
                            
                          	<div class="form-group" id="purchaseTimeDiv" style="display: none">
                               	<label class="col-md-3 control-label" for="purchaseTime">Purchase Time:</label>
                               	<div class="col-md-7">
                               		<input type="text" id="purchaseTime" name="purchaseTime" class="form-control datetime" placeholder="YYYY-MM-DD HH:mm:ss">
                               	</div>
                            </div>
                            
                            <div class="form-group">
                            	<label class="col-md-3 control-label">Batch Calls:</label>
                              	<div class="col-md-7">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="charge" name="charge" value="Charge"> Charge
                                        </label>
                                        <label>
                                            <input type="checkbox" id="refund" name="refund" value="Refund"> Refund
                                        </label>
                                        <label>
                                            <input type="checkbox" id="cancel" name="cancel" value="Cancel"> Cancel
                                        </label>
                                    </div>
                              	</div>
                            </div>
                            <div class="form-group">
                            	<div class="col-md-offset-3 col-md-7">
                                    <input type="submit" class="btn btn-primary" id="runManualTestBtn" name ="runManualTest" value="Run Manual Test" data-loading-text="Running..." />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
    $(document).ready(function() {
        $('.datetime').datetimepicker({
        	format: "YYYY-MM-DD HH:mm:ss",
        	autoclose:true,
        	useSeconds: true,
        	todayHighlight: true,
        	todayBtn: true
        });
    	
        var $manualTestingForm = $('#manualTestingForm');
        var $runManualTestBtn = $('#runManualTestBtn');
        
        $manualTestingForm.validate({
            submitHandler: function() {
                $runManualTestBtn.button('loading');
                $.ajax({
                    type: 'POST',
                    url: 'npp/runManualTest',
                    data: $manualTestingForm.serializeArray(),
                    success: function(response) {
                    	redirect(response);
                    }
                });
                return false;
            }
        });
        var redirect = function(response) {
        	var projectName = window.location.pathname.split('/')[1];
        	window.location.href = 'http://'+window.location.host+'/'+projectName+'/'+response;
        }
        

        $(".numeric").numeric();
        var $totalDiv = $manualTestingForm.find("#totalDiv");
        var $dcbTosVersionDiv = $manualTestingForm.find("#dcbTosVersionDiv");
        var $subscriptionIdDiv = $manualTestingForm.find("#subscriptionIdDiv");
        var $subscriptionRecurrenceDiv = $manualTestingForm.find("#subscriptionRecurrenceDiv");
        var $purchaseTimeDiv = $manualTestingForm.find("#purchaseTimeDiv");
        
        $manualTestingForm.find('#auth').change(function () {
            if ($(this).prop('checked') == true) {
                $totalDiv.show();
                $("#total").attr("required", "required");

                $dcbTosVersionDiv.show();
                $("#dcbTosVersion").attr("required", "required");

                $subscriptionIdDiv.show();
                $subscriptionRecurrenceDiv.show();
                $purchaseTimeDiv.show();
            } else {
                $totalDiv.hide();
                $("#total").removeAttr("required");

                $dcbTosVersionDiv.hide();
                $("#dcbTosVersion").removeAttr("required");

                $subscriptionIdDiv.hide();
                $("#subscriptionId").val("");

                $subscriptionRecurrenceDiv.hide();
                $("#subscriptionRecurrence").val("");
                $("#subscriptionRecurrence").addClass('placeholder');
                
                
                $purchaseTimeDiv.hide();
                $("#purchaseTime").val("");
            }
        });
        $('select').change(function() {
        	 if ($(this).children('option:first-child').is(':selected')) {
        	   $(this).addClass('placeholder');
        	 } else {
        	  $(this).removeClass('placeholder');
        	 }
        });
    });
</script>
</html>