<!DOCTYPE html>
<html lang="en">
    <head>
        <%@include file="/WEB-INF/header.jsp" %>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Test Result Details</title>
    </head>
    <body>
        <!-- Page Content -->
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="list-group">
                        <a href="npp/history" class="list-group-item active">History</a>
                        <a href="npp/echo" class="list-group-item">Echo Testing</a>
                        <a href="npp/manualTesting" class="list-group-item">Manual Testing</a>
                        <a href="npp/check" class="list-group-item">Check Request File</a>
                        <a href="npp/editEnvironment" class="list-group-item">Edit Environment</a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="thumbnail">
                        <div class="caption-full">
                            <div class="table-responsive">
                                <div>
                                    <center>DCB 3.0 Calls</center>
                                    <table class="table table-bordered">
                                        <tr class="active">
                                            <th>Correlation Id</th>
                                            <th>Call</th>
                                            <th>Result</th>
                                            <th>User Message</th>
                                        </tr>
                                        <c:forEach var="soapRecord" begin="0" items="${requestScope.soapRecord}" varStatus="status">
                                        <tr>
                                            <td name="correlationId">${soapRecord.correlationId}</td>
                                            <td name="soapCall">${soapRecord.soapCall}</td>
                                            <td name="result${status.count}">${soapRecord.result}</td>
                                            <td name="userMessage${status.count}">${soapRecord.userMessage}</td>
                                        </tr>
                                        </c:forEach>
                                    </table>
                                </div>
                            </div>
                            <c:forEach var="soapRecord" begin="0" items="${requestScope.soapRecord}">
                            <div class="form-group">
                                <label for="comment"><font size="6">${soapRecord.soapCall} Soap Message</font></label>
                                <textarea class="form-control" rows="11" id="comment">${soapRecord.sendMessage}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="comment"><font size="6">${soapRecord.soapCall} Soap Response</font></label>
                                <textarea class="form-control" rows="11" id="comment">${soapRecord.receiveMessage}</textarea>
                            </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>