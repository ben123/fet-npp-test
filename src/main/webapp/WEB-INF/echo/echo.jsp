<html lang="en">
<head>
<%@include file="/WEB-INF/header.jsp" %>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>Echo Testing</title>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="list-group">
					<a href="npp/history" class="list-group-item">History</a> 
					<a href="npp/echo" class="list-group-item active">Echo Testing</a> 
					<a href="npp/manualTesting" class="list-group-item">Manual Testing</a> 
					<a href="npp/check" class="list-group-item">Check Request File</a>
					<a href="npp/editEnvironment" class="list-group-item">Edit Environment</a>
				</div>
			</div>
			<div class="col-md-9">
				<div class="thumbnail">
					<div class="caption-full">
						<form class="form-horizontal" role="form" action="npp/runEchoTest" method="POST">
						
							<div class="form-group">
                        		<label class="col-md-3 control-label" for="userMessage">Echo Testing</label>
								<div class="col-md-7">
									<div class="input-group">
								      	<input id="userMessage" type="text" maxLength="255" name="userMessage" class="form-control" placeholder="Text input">
								      	<span class="input-group-btn">
								      		<input class="btn btn-primary" type="submit" value="Echo!"/>
								      	</span>
							    	</div>
							  	</div>
                     		</div>
                     		
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>