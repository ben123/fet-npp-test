DROP SEQUENCE IF EXISTS CORRELATION_ID_SEQ;
DROP SEQUENCE IF EXISTS PAYMENT_ID_SEQ;
DROP TABLE IF EXISTS TESTCASE_HISTORY;
DROP TABLE IF EXISTS SOAP_RECORD;
DROP TABLE IF EXISTS REQ_FILE_LOG;
DROP TABLE IF EXISTS APP_ENV;

CREATE SEQUENCE CORRELATION_ID_SEQ;
CREATE SEQUENCE PAYMENT_ID_SEQ;
CREATE TABLE TESTCASE_HISTORY(ID BIGINT AUTO_INCREMENT, CORRELATION_ID VARCHAR(20),REQ_ID VARCHAR(20), OPERATOR_USER_TOKEN VARCHAR(255), SOAP_CALLS VARCHAR(50), BATCH_CALLS VARCHAR(50), ITEM_PRICE LONG,TX_STATUS VARCHAR(10), CREATION_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
CREATE TABLE SOAP_RECORD(CORRELATION_ID VARCHAR(20),SEND_MESSAGE CLOB,RECEIVE_MESSAGE CLOB, RESULT VARCHAR(25), USER_MESSAGE VARCHAR(255), SOAP_CALL VARCHAR(50), CREATION_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
CREATE TABLE REQ_FILE_LOG(REQ_ID VARCHAR(20),FILE_NAME VARCHAR(200), CREATION_TIME TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
CREATE TABLE APP_ENV(ENV_NAME VARCHAR(50) PRIMARY KEY,ENV_VALUE VARCHAR(250));