package fet.npp.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import fet.npp.dao.InternalDBStore;
import fet.npp.utils.DateUtils;

/**
 *
 * @author bill
 */
@Service
public class PaymentFileService {

    @Value("${ftp.ip}")
    String server;

    @Value("${ftp.port}")
    String port;

    @Value("${ftp.user}")
    String user;

    @Value("${ftp.password}")
    String password;

    @Value("${ftp.upload.directory}")
    String directory;

    @Autowired
    InternalDBStore iDBStore;

    final FTPClient ftpClient = new FTPClient();

    public String makeCsvFileAndUpload(final List<String[]> params) {
        final String seqId = iDBStore.getPaymentIdSeq();
        final String unixTime = DateUtils.getCurrentUnixTime();
        final String fileName = getFileName(unixTime, seqId);
        final String fileContent = getFileContent(params, unixTime, seqId);
        final String fullPath= getFullDirectories(unixTime);
        upload(fileName, fileContent, seqId, fullPath);
        return seqId;
    }

    private String getFileName(final String unixTime, final String seqId) {
        Date time = new Date(Long.parseLong(unixTime));
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        return String.format("request_FET_TW_%s-0700_%s.csv", df.format(time), seqId);
    }

    private String getFileContent(final List<String[]> params, final String unixTime, final String seqId) {
        final StringWriter sw = new StringWriter();
        sw.append(String.format("REQUESTFILE,%s,%s,FET_TW", unixTime, seqId));
        for (String[] array : params) {
            sw.append(String.format("\n%s,%s,%s,FET_TW", array[0], unixTime, array[1]));
        }
        return sw.toString();
    }
    
    private String getFullDirectories(final String unixTime) {
        Date time = new Date(Long.parseLong(unixTime));
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
//        return String.format("/%s%s/", directory, df.format(time));
        return directory;
    }

    private void upload(final String fileName, final String fileContent, final String seqId, final String fullPath) {
        try {
            connect();
            
            ftpClient.makeDirectory(fullPath);
            
            
            final String remote = String.format("%s%s", fullPath, fileName);
            final InputStream inputStream = new ByteArrayInputStream(fileContent.getBytes());
            ftpClient.storeFile(remote, inputStream);
            inputStream.close();
            iDBStore.saveRequestFileLog(Integer.valueOf(seqId), remote);
        } catch (IOException ex) {
            Logger.getLogger(PaymentFileService.class.getName()).log(Level.SEVERE, null, ex);
//            throw new RuntimeException("something wrong when connecting ftp server.");
        } finally {
            try {
                disconnect();
            } catch (IOException ex) {
                Logger.getLogger(PaymentFileService.class.getName()).log(Level.SEVERE, null, ex);
//                throw new RuntimeException("something wrong when disconnecting ftp server.");
            }
        }
    }

    private void connect() throws IOException {
        ftpClient.setConnectTimeout(5000);
        ftpClient.connect(server, Integer.parseInt(port));
        ftpClient.login(user, password);
        ftpClient.enterLocalPassiveMode();
    }

    private void disconnect() throws IOException {
        if (ftpClient.isConnected()) {
            ftpClient.logout();
            ftpClient.disconnect();
        }
    }
}
