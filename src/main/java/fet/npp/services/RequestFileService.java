package fet.npp.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import fet.npp.dao.InternalDBStore;
/**
*
* @author ben
*/
@Service
@EnableScheduling
public class RequestFileService {
	@Autowired
    InternalDBStore internalDBStore;
	
	@Autowired
    PaymentFileService pfService;
	
	@Scheduled(fixedDelayString = "${batch.fixed.delay.prop}")
	public void createRequestFile() {
		// create CSV file to FTP Server
		System.out.println("test schedule.....");
		System.out.println("date: "+ new Date());
		
		final List<Map<String, Object>> rows = internalDBStore.queryNoFileHistory();
		final List<String[]> requestFileData = new ArrayList<String[]>();
		final List<Long> ids = new ArrayList<Long>();
        for (int i = 0; i < rows.size(); i++) {
            final Map<String, Object> row = rows.get(i);

            final long id = (long) row.get("ID");
            final String correlationId = (String) row.get("CORRELATION_ID");
            final String batchCalls = (String) row.get("BATCH_CALLS");
            
            ids.add(id);
            String[] soapCallsArr = batchCalls.split(",");
        	for (String soapCall : soapCallsArr) {
        		requestFileData.add(new String[]{StringUtils.upperCase(soapCall), "" + correlationId});
        	}
        }
        if (!requestFileData.isEmpty()) {
        	String reqId = pfService.makeCsvFileAndUpload(requestFileData);
        	for (Long id : ids) {
        		internalDBStore.updateFileHistory(reqId, id);
        	}
        	internalDBStore.savelastBatchTime();
        }
        
	}
}