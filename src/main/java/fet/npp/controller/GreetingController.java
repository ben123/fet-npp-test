package fet.npp.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fet.ws.npp.Auth;
import com.fet.ws.npp.AuthResponse;
import com.fet.ws.npp.Echo;
import com.fet.ws.npp.EchoResponse;
import com.fet.ws.npp.GetProvisioning;
import com.fet.ws.npp.GetProvisioningResponse;

import fet.npp.dao.InternalDBStore;
import fet.npp.dao.RequestFileDetailEntity;
import fet.npp.dao.RequestFileHistoryEntity;
import fet.npp.dao.SoapRecordEntity;
import fet.npp.dao.TestCaseHistoryEntity;
import fet.npp.services.PaginationService;
import fet.npp.services.PaymentFileService;
import fet.npp.services.RequestFileService;
import fet.npp.webService.CarrierBillingClient;

/**
 *
 * @author Karl, Ben
 *
 */
@Controller
public class GreetingController {

    private static final Logger log = LoggerFactory.getLogger(GreetingController.class);
    final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private ScheduledFuture<?> future;
    //private ThreadPoolTaskScheduler scheduler;

    @Autowired
    InternalDBStore internalDBStore;

    @Autowired
    PaymentFileService pfService;
    
    @Autowired
    RequestFileService rfService;

    @Autowired
    CarrierBillingClient carrierBillingClient;
    
    @Autowired
    PaginationService paginationService;

    @RequestMapping(value = {"/home", "/history"})
    public String index() {
    	final String defaultPage = "1";
        return "redirect:/npp/history/testResultList/0/0/0/0/0/" + defaultPage;
    }
    
    @RequestMapping(value = {"/history/testResultList/{out}/{correlationIdFrom}/{correlationIdTo}/{creationTimeFrom}/{creationTimeTo}/{page}"})
    public String getTestResultListSearch(@PathVariable String out, @PathVariable String correlationIdFrom, @PathVariable String correlationIdTo, @PathVariable String creationTimeFrom, @PathVariable String creationTimeTo, @PathVariable String page, @RequestParam Map<String, Object> params, Model model) {
    	log.info("getHistorySearchList params:{}", params);
    	
    	final int clickPage = Integer.parseInt(page);

        final int rowsShowCount = 20;
        final int pageIndexCount = 10;
    	
    	out = "0".equals(out) ? null : out;
    	correlationIdFrom = "0".equals(correlationIdFrom) ? null : correlationIdFrom;
    	correlationIdTo = "0".equals(correlationIdTo) ? null : correlationIdTo;
    	creationTimeFrom = "0".equals(creationTimeFrom) ? null : creationTimeFrom;
    	creationTimeTo = "0".equals(creationTimeTo) ? null : creationTimeTo;
    	
    	final List<TestCaseHistoryEntity> historyList = internalDBStore.queryHistoryList(out, correlationIdFrom, correlationIdTo, creationTimeFrom, creationTimeTo, clickPage, rowsShowCount);
    	log.info("result:{}", historyList);
    	log.info("result size:{}", historyList.size());
    	for (int i = 0 ; i < historyList.size() ; i ++) {
    		final TestCaseHistoryEntity row = historyList.get(i);
    		log.info("row:{}", ToStringBuilder.reflectionToString(row));
    	}
    	model.addAttribute("historyList", historyList);
    	model.addAllAttributes(paginationService.pagination(clickPage, historyList.isEmpty()? 0 : historyList.get(0).getTotalCount(), rowsShowCount, pageIndexCount));
    	model.addAttribute("out", null == out? "0" : out);
    	model.addAttribute("correlationIdFrom", null == correlationIdFrom? "0" : correlationIdFrom);
    	model.addAttribute("correlationIdTo", null == correlationIdTo? "0" : correlationIdTo);
    	model.addAttribute("creationTimeFrom", null == creationTimeFrom? "0" : creationTimeFrom);
    	model.addAttribute("creationTimeTo", null == creationTimeTo? "0" : creationTimeTo);

        return "history/testResultList";
    }

    @RequestMapping("/history/testResultDetails/{correlationId}")
    public String getTestResultDetails(@PathVariable String correlationId, Model model) {
        log.info("getTestResultDetails correlationId:{}", correlationId);
        final List<SoapRecordEntity> soapRecordEntityList = internalDBStore.querySoapRecordList(correlationId);
        model.addAttribute("soapRecord", soapRecordEntityList);
        return "history/testResultDetails";
    }

    @RequestMapping(value = "/runManualTest", method = RequestMethod.POST)
//    @Transactional
    @ResponseBody
    public String runManualTest(@RequestParam Map<String, Object> params) throws ParseException {
        log.info("runManualTest params:{}", params);
        final String out = (String) params.get("out");

        final String provision = (String) params.get("provision");
        final String auth = (String) params.get("auth");

        final String soapCalls = stringJoin(new String[]{provision, auth}, ",");

        final String total = StringUtils.isNotEmpty((String) params.get("total")) ? (String) params.get("total") : null;
        final String dcbTosVersion = (String) params.get("dcbTosVersion");
        final String subscriptionId = (String) params.get("subscriptionId");
        final String subscriptionRecurrence = (String) params.get("subscriptionRecurrence");
        final Date purchaseTime = StringUtils.isNotEmpty((String) params.get("purchaseTime")) ? simpleDateFormat.parse((String) params.get("purchaseTime")) : new Date();

        final String charge = (String) params.get("charge");
        final String refund = (String) params.get("refund");
        final String cancel = (String) params.get("cancel");
        final String batchCalls = stringJoin(new String[]{charge, refund, cancel}, ",");

        // get correlationId from db-sequence if UI undefined correlationId
        final String correlationId = StringUtils.isNotEmpty((String) params.get("correlationId")) ? (String) params.get("correlationId") : internalDBStore.getCorrelationIdBySequence();
        
        // invoke NPP API
        if (StringUtils.isNotEmpty(provision)) {
            final GetProvisioning getProvisioningRequest = carrierBillingClient.getGetProvisioningRequest(correlationId,
                    out);
            final GetProvisioningResponse getProvisioningResponse = carrierBillingClient
                    .getProvisioning(getProvisioningRequest);
            final String getProvisioningRequestXml = carrierBillingClient.marshal(getProvisioningRequest,
                    "getProvisioning", GetProvisioning.class);
            final String getProvisioningResponseXml = carrierBillingClient.marshal(getProvisioningResponse,
                    "getProvisioningResponse", GetProvisioningResponse.class);
            // save request/response
            final String result = getProvisioningResponse.getMessage();
            final String userMessage = getProvisioningResponse.getUserMessage();
            internalDBStore.saveSoapRecord(correlationId, getProvisioningRequestXml, getProvisioningResponseXml, result, userMessage, provision);
        }
        String TX_STATUS = null;
        if (StringUtils.isNotEmpty(auth)) {
            final Auth authRequest = carrierBillingClient.getAuthRequest(correlationId, out, total, dcbTosVersion, subscriptionId, subscriptionRecurrence, purchaseTime);
            final AuthResponse authResponse = carrierBillingClient.auth(authRequest);
            final String authRequestXml = carrierBillingClient.marshal(authRequest, "auth", Auth.class);
            final String authResponseXml = carrierBillingClient.marshal(authResponse, "authResponse",
                    AuthResponse.class);
            
            // save request/response
            final String result = authResponse.getMessage();
            if("SUCCESS".equals(result)) {
            	TX_STATUS = "A";
            } else {
            	TX_STATUS = "FA";
            }
            final String userMessage = authResponse.getUserMessage();
            internalDBStore.saveSoapRecord(correlationId, authRequestXml, authResponseXml, result, userMessage, auth);
        }
        
        
        // save testCaseData
        internalDBStore.saveTestCase(correlationId, soapCalls, batchCalls, out, total, TX_STATUS);
        
        return String.format("npp/history/testResultDetails/%s", correlationId);

    }

    @RequestMapping("/manualTesting")
    public String getManualTesting(Model model) {
    	final Map<String, Object> envConfig = internalDBStore.queryEnvConfig();
    	for (String key : envConfig.keySet()) {
        	model.addAttribute(key, (String) envConfig.get(key));
    	}
        return "manualTesting";
    }
    
    @RequestMapping(value = {"/check"})
    public String getRequestFileListIndex() {
    	final String defaultPage = "1";
        return "redirect:/npp/check/0/0/0/" + defaultPage;
    }
    
    @RequestMapping("/check/{out}/{correlationIdFrom}/{correlationIdTo}/{page}")
    public String getRequestFileList(@PathVariable String out, @PathVariable String correlationIdFrom, @PathVariable String correlationIdTo, @PathVariable String page, @RequestParam Map<String, Object> params, Model model) {
    	log.info("getRequestFileList params:{}", params);
        final int clickPage = Integer.parseInt(page);
        final int rowsShowCount = 20;
        final int pageIndexCount = 10;
        
        out = "0".equals(out) ? null : out;
    	correlationIdFrom = "0".equals(correlationIdFrom) ? null : correlationIdFrom;
    	correlationIdTo = "0".equals(correlationIdTo) ? null : correlationIdTo;
    	
    	final List<RequestFileHistoryEntity> requestFileList = internalDBStore.queryRequestFileList(out, correlationIdFrom, correlationIdTo, clickPage, rowsShowCount);
    	log.info("result:{}", requestFileList);
    	log.info("result size:{}", requestFileList.size());
    	for (int i = 0 ; i < requestFileList.size() ; i ++) {
    		final RequestFileHistoryEntity row = requestFileList.get(i);
    		log.info("row:{}", ToStringBuilder.reflectionToString(row));
    	}
    	model.addAttribute("requestFileList", requestFileList);
        model.addAllAttributes(paginationService.pagination(clickPage, requestFileList.isEmpty()? 0 : requestFileList.get(0).getTotalCount(), rowsShowCount, pageIndexCount));
        model.addAttribute("out", null == out? "0" : out);
    	model.addAttribute("correlationIdFrom", null == correlationIdFrom? "0" : correlationIdFrom);
    	model.addAttribute("correlationIdTo", null == correlationIdTo? "0" : correlationIdTo);
    	
        return "/check/requestFileList";
    }
    
    @RequestMapping("/check/fileDetails/{fileDetailId}")
    public String queryFileDetails(@PathVariable String fileDetailId, Model model) {
        log.info("queryFileDetails fileDetailId:{}", fileDetailId);
        final List<RequestFileDetailEntity> requestFileDetailList = internalDBStore.queryRequestFileDetail(fileDetailId);
        model.addAttribute("requestFileDetail", requestFileDetailList);
        return "/check/requestFileDetails";
    }
    
    @RequestMapping("/echo")
    public String echo() {
        log.info("echo {}","<<<<<<<<<<<<<");      
        return "/echo/echo";
    }
    
    @RequestMapping(value = "/runEchoTest", method = RequestMethod.POST)
    public String sendEcho(@RequestParam String userMessage) {
    	log.info("echo userMessage :{}", userMessage);
    	//collect frondEnd parameters
    	final String echoUserMessage = userMessage.isEmpty() ? "" : userMessage;
    	final String correlationId = internalDBStore.getCorrelationIdBySequence();
    	
    	//invoke WebService
    	Echo echoRequest = carrierBillingClient.getEchoRequest(correlationId, echoUserMessage);
    	EchoResponse echoResponse = carrierBillingClient.echo(echoRequest);
    	
    	//transaction 2 xml
    	 final String echoRequestXml = carrierBillingClient.marshal(echoRequest,
                 "echoRequest", Echo.class);
         final String echoResponseXml = carrierBillingClient.marshal(echoResponse,
                 "echoResponse", EchoResponse.class);
         
      // save request/response
         final String originalMessage = echoResponse.getOriginalMessage();
         final String echoResponseResult = echoResponse.getResult().toString();
         
         internalDBStore.saveSoapRecord(correlationId, echoRequestXml, echoResponseXml, echoResponseResult, originalMessage, "echo");
         return String.format("redirect:/npp/history/testResultDetails/%s", correlationId);
    }
    
    @RequestMapping("/saveEnvironmentConfig")
    public String saveEnvironmentConfig(@RequestParam Map<String, Object> params) {
    	log.info("editEnvironment params:{}", params);
    	log.info("editEnvironment keySet:{}", params.keySet());
    	
    	internalDBStore.saveEnvConfig(params);
        return "redirect:/npp/editEnvironment";
    }
    
    @RequestMapping("/editEnvironment")
    public String editEnvironment(Model model) {
    	final Map<String, Object> envConfig = internalDBStore.queryEnvConfig();
    	for (String key : envConfig.keySet()) {
        	model.addAttribute(key, (String) envConfig.get(key));
    	}
        return "/editEnvironment";
    }
    
    @RequestMapping("/runRequestFile")
	public String runRequestFile() {
		rfService.createRequestFile();
		return "redirect:/npp/editEnvironment";
	}
    
    private String stringJoin(final String[] array, final String separator) {
        final StringBuffer strJoin = new StringBuffer();
        for (int i = 0; i < array.length; i++) {
            final String str = array[i];
            if (StringUtils.isNotEmpty(str)) {
                strJoin.append(str).append(separator);
            }
        }
        return StringUtils.isNotEmpty(strJoin.toString()) ? StringUtils.removeEnd(strJoin.toString(), separator) : null;
    }
}
