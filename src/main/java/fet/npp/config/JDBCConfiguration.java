package fet.npp.config;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.h2.jdbcx.JdbcDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 *
 * @author Karl
 *
 */
@Configuration
public class JDBCConfiguration {

	private JdbcTemplate jdbcTemplateForH2;
	private NamedParameterJdbcTemplate namedParameterJdbcTemplateForH2;
	@Value("${h2.datasource.url}")
	String h2Url;
	@Value("${h2.datasource.username}")
	private String h2Username;
	@Value("${h2.datasource.password}")
	private String h2Password;

	private JdbcTemplate jdbcTemplateForDB2;
	private NamedParameterJdbcTemplate namedParameterJdbcTemplateForDB2;
	@Value("${db2.datasource.driverClassName}")
	String db2DriverClassName;
	@Value("${db2.datasource.url}")
	String db2Url;
	@Value("${db2.datasource.username}")
	private String db2Username;
	@Value("${db2.datasource.password}")
	private String db2Password;

	@Bean(name = "jdbcTemplateForH2")
	public JdbcTemplate getJdbcTemplateForH2() {
		return jdbcTemplateForH2;
	}

	@Bean(name = "jdbcTemplateForDB2")
	public JdbcTemplate getJdbcTemplateForDB2() {
		return jdbcTemplateForDB2;
	}

	@Bean(name = "namedParameterJdbcTemplateForH2")
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplateForH2() {
		return namedParameterJdbcTemplateForH2;
	}

	@Bean(name = "namedParameterJdbcTemplateForDB2")
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplateForDB2() {
		return namedParameterJdbcTemplateForDB2;
	}

	@Autowired
	@Qualifier("dataSourceForH2")
	public void setJdbcTemplateForH2(DataSource dataSourceForH2) {
		this.jdbcTemplateForH2 = new JdbcTemplate(dataSourceForH2);
		this.namedParameterJdbcTemplateForH2 = new NamedParameterJdbcTemplate(dataSourceForH2);
	}

	@Autowired
	@Qualifier("dataSourceForDB2")
	public void setJdbcTemplateForDB2(DataSource dataSourceForDB2) {
		this.jdbcTemplateForDB2 = new JdbcTemplate(dataSourceForDB2);
		this.namedParameterJdbcTemplateForDB2 = new NamedParameterJdbcTemplate(dataSourceForDB2);
	}

	@Primary
	@Bean(name = "dataSourceForH2")
	public DataSource dataSourceForH2() {
		final JdbcDataSource jdbcDataSource = new JdbcDataSource();
		jdbcDataSource.setURL(h2Url);
		jdbcDataSource.setUser(h2Username);
		jdbcDataSource.setPassword(h2Password);

		return jdbcDataSource;
	}

	@Bean(name = "dataSourceForDB2")
	public DataSource dataSourceForDB2() {
		final BasicDataSource basicDataSource = new BasicDataSource();
		basicDataSource.setDriverClassName(db2DriverClassName);
		basicDataSource.setUrl(db2Url);
		basicDataSource.setUsername(db2Username);
		basicDataSource.setPassword(db2Password);

		return basicDataSource;
	}
}
