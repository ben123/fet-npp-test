package fet.npp.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.wss4j.dom.handler.WSHandlerConstants;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.ws.config.annotation.EnableWs;

import fet.npp.dummyServer.CarrierBillingSOAPImpl;

/**
 *
 * @author Karl
 *
 */
@Configuration
@EnableWs
@ImportResource({ "classpath:META-INF/cxf/cxf.xml"})
public class WSConfiguration extends WebMvcConfigurerAdapter {

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private WSS4JInInterceptor loggingInInterceptor;
	@Autowired
	private WSS4JOutInterceptor loggingOutInterceptor;


    // Replaces the need for web.xml
    


	// Replaces cxf-servlet.xml dummy server setting
	@Bean
	public EndpointImpl CarrierBilling() {
		final Bus bus = (Bus) applicationContext.getBean(Bus.DEFAULT_BUS_ID);
		final Object implementor = new CarrierBillingSOAPImpl();
		final EndpointImpl endpoint = new EndpointImpl(bus, implementor);
		endpoint.publish("/CarrierBillingSOAP");

		// decrypt
		endpoint.getServer().getEndpoint().getInInterceptors().add(loggingInInterceptor);
		// sign
		// endpoint.getServer().getEndpoint().getOutInterceptors().add(loggingOutInterceptor);

		return endpoint;
	}

	@Bean
	public WSS4JInInterceptor decrypt() {
		final Map<String, Object> inProps = new HashMap<String, Object>();
		inProps.put("action", "Timestamp Signature");
		inProps.put(WSHandlerConstants.SIG_PROP_FILE, "client-crypto.properties");
		inProps.put("passwordCallbackClass", "fet.npp.webService.ClientCallback");
		final WSS4JInInterceptor loggingInInterceptor = new WSS4JInInterceptor(inProps);
		return loggingInInterceptor;
	}

	@Bean
	public WSS4JOutInterceptor sign() {
		final Map<String, Object> outProps = new HashMap<String, Object>();
		outProps.put("action", "Timestamp Signature");
		outProps.put("user", "selfsigned");
		outProps.put(WSHandlerConstants.SIG_PROP_FILE, "client-crypto.properties");
		outProps.put("signatureKeyIdentifier", "DirectReference");
		outProps.put("signatureKeyIdentifier", "IssuerSerial");
		outProps.put("passwordCallbackClass", "fet.npp.webService.ClientCallback");
		outProps.put("enableSignatureConfirmation", "true");
		outProps.put("signatureParts",
				"{Element}{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd}Timestamp;{Element}{http://schemas.xmlsoap.org/soap/envelope/}Body");
		final WSS4JOutInterceptor loggingOutInterceptor = new WSS4JOutInterceptor(outProps);
		return loggingOutInterceptor;
	}

}
