package fet.npp.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource({"classpath:/config.xml"})
@ComponentScan({"fet.npp.dao", "fet.npp.webService", "fet.npp.services"})
public class AppConfig {

}
