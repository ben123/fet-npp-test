package fet.npp.dao;

/**
 *
 * @author Ben
 *
 */
public class RequestFileHistoryEntity {

	private String operatorUserToken;
	private String correlationId;
	private String requestFileId;
	private String requestFileName;
	private String fileDetailId;
	private String status;
	private String creationTime;
	private String batchCalls;
	private int totalCount;

	public String getOperatorUserToken() {
		return operatorUserToken;
	}

	public void setOperatorUserToken(String operatorUserToken) {
		this.operatorUserToken = operatorUserToken;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getRequestFileId() {
		return requestFileId;
	}

	public void setRequestFileId(String requestFileId) {
		this.requestFileId = requestFileId;
	}

	public String getRequestFileName() {
		return requestFileName;
	}

	public void setRequestFileName(String requestFileName) {
		this.requestFileName = requestFileName;
	}

	public String getFileDetailId() {
		return fileDetailId;
	}

	public void setFileDetailId(String fileDetailId) {
		this.fileDetailId = fileDetailId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public String getBatchCalls() {
		return batchCalls;
	}

	public void setBatchCalls(String batchCalls) {
		this.batchCalls = batchCalls;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
}
