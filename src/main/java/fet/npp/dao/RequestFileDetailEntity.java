package fet.npp.dao;

/**
 *
 * @author Ben
 *
 */
public class RequestFileDetailEntity {

	private String fileDetailId;
	private String requestType;
	private String correlationId;
	private String status;

	public String getFileDetailId() {
		return fileDetailId;
	}

	public void setFileDetailId(String fileDetailId) {
		this.fileDetailId = fileDetailId;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
