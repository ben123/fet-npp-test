package fet.npp.dao;

/**
 *
 * @author Ben
 *
 */
public class TestCaseHistoryEntity {

    private String correlationId;

    private String operatorUserToken;

    private String soapCalls;

    private String batchCalls;

    private Long itemPrice;

    private String creationTime;
    
    private int totalCount;

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getOperatorUserToken() {
        return operatorUserToken;
    }

    public void setOperatorUserToken(String operatorUserToken) {
        this.operatorUserToken = operatorUserToken;
    }

    public String getSoapCalls() {
        return soapCalls;
    }

    public void setSoapCalls(String soapCalls) {
        this.soapCalls = soapCalls;
    }

    public String getBatchCalls() {
        return batchCalls;
    }

    public void setBatchCalls(String batchCalls) {
        this.batchCalls = batchCalls;
    }

    public Long getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(Long itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
