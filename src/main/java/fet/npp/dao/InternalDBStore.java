package fet.npp.dao;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author Ben
 *
 */
@Component
public class InternalDBStore {
    private static final Logger log = LoggerFactory.getLogger(InternalDBStore.class);

    final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    @Autowired
    @Qualifier(value = "jdbcTemplateForH2")
    private JdbcTemplate jdbcTemplateForH2;
    @Autowired
    @Qualifier(value = "namedParameterJdbcTemplateForH2")
    private NamedParameterJdbcTemplate namedParameterJdbcTemplateForH2;

    @Autowired
    @Qualifier(value = "jdbcTemplateForDB2")
    private JdbcTemplate jdbcTemplateForDB2;
    @Autowired
    @Qualifier(value = "namedParameterJdbcTemplateForDB2")
    private NamedParameterJdbcTemplate namedParameterJdbcTemplateForDB2;

    public void saveTestCase(final String correlationId, final String soapCalls,
        final String batchCalls, final String out, final String total, final String TX_STATUS) {
        if (StringUtils.isNotEmpty(out)) {
            final String sql = "insert into TESTCASE_HISTORY(CORRELATION_ID, SOAP_CALLS, BATCH_CALLS, OPERATOR_USER_TOKEN, ITEM_PRICE, TX_STATUS) values(:CORRELATION_ID, :SOAP_CALLS, :BATCH_CALLS, :OPERATOR_USER_TOKEN, :ITEM_PRICE, :TX_STATUS)";

            final MapSqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("CORRELATION_ID", correlationId)
                    //.addValue("REQ_ID", requestFileId)
                    .addValue("SOAP_CALLS", soapCalls)
                    .addValue("BATCH_CALLS", batchCalls)
                    .addValue("OPERATOR_USER_TOKEN", out)
                    .addValue("ITEM_PRICE", StringUtils.isNotEmpty(total) ? Long.valueOf(total) : total)
            		.addValue("TX_STATUS", TX_STATUS);
            log.info("sql:[{}],parameters:[{}]", sql, parameters.getValues());
            namedParameterJdbcTemplateForH2.update(sql, parameters);
        }
    }

    public void saveSoapRecord(final String correlationId, final String sendMessage, final String receiveMessage,
        final String result, final String userMessage, final String soapCall) {
        final String sql = "insert into SOAP_RECORD(CORRELATION_ID, SEND_MESSAGE, RECEIVE_MESSAGE, RESULT, USER_MESSAGE, SOAP_CALL) values(:CORRELATION_ID, :SEND_MESSAGE, :RECEIVE_MESSAGE, :RESULT, :USER_MESSAGE, :SOAP_CALL)";

        final MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("CORRELATION_ID", correlationId)
            .addValue("SEND_MESSAGE", sendMessage)
            .addValue("RECEIVE_MESSAGE", receiveMessage)
            .addValue("RESULT", result)
            .addValue("USER_MESSAGE", userMessage)
            .addValue("SOAP_CALL", soapCall);

        log.info("sql:[{}],parameters:[{}]", sql, parameters.getValues());
        namedParameterJdbcTemplateForH2.update(sql, parameters);
    }

    public void saveRequestFileLog(final int reqId, final String fileName) {
        final String sql = "insert into REQ_FILE_LOG(REQ_ID, FILE_NAME) values(:REQ_ID, :FILE_NAME)";

        final MapSqlParameterSource parameters = new MapSqlParameterSource()
            .addValue("REQ_ID", reqId)
            .addValue("FILE_NAME", fileName);

        log.info("sql:[{}],parameters:[{}]", sql, parameters.getValues());
        namedParameterJdbcTemplateForH2.update(sql, parameters);
    }

    public String getCorrelationIdBySequence() {
        final String sql = "select CORRELATION_ID_SEQ.nextval as correlation_id from dual";

        log.info("sql:[{}]", sql);
        final String correlationId = jdbcTemplateForH2.queryForObject(sql, String.class);
        return correlationId;
    }

    public int queryHistoryRowsCount() {
        final String sql = "SELECT * FROM TESTCASE_HISTORY";
        log.info("sql:[{}]", sql);
        return jdbcTemplateForH2.queryForList(sql).size();
    }

    public List<TestCaseHistoryEntity> queryHistoryList(final String out, final String correlationIdFrom,
        final String correlationIdTo, final String creationTimeFrom, final String creationTimeTo, final int page, final int count) {
        final StringBuilder sql = new StringBuilder("SELECT * FROM TESTCASE_HISTORY WHERE 0=0 ");

        final MapSqlParameterSource parameters = new MapSqlParameterSource();

        boolean alLeastOneNotEmpty = false;
        if (StringUtils.isNotEmpty(out)) {
            alLeastOneNotEmpty = true;
            sql.append("AND OPERATOR_USER_TOKEN = :OPERATOR_USER_TOKEN ");
            parameters.addValue("OPERATOR_USER_TOKEN", out);
        }
        if (StringUtils.isNotEmpty(correlationIdFrom)) {
            alLeastOneNotEmpty = true;
            sql.append("AND CORRELATION_ID >= :CORRELATION_ID_FROM ");
            parameters.addValue("CORRELATION_ID_FROM", Long.parseLong(correlationIdFrom));
        }
        if (StringUtils.isNotEmpty(correlationIdTo)) {
            alLeastOneNotEmpty = true;
            sql.append("AND CORRELATION_ID <= :CORRELATION_ID_TO ");
            parameters.addValue("CORRELATION_ID_TO", Long.parseLong(correlationIdTo));
        }
        if (StringUtils.isNotEmpty(creationTimeFrom)) {
            alLeastOneNotEmpty = true;
            sql.append("AND CREATION_TIME >= :CREATION_TIME_FROM ");
            parameters.addValue("CREATION_TIME_FROM", creationTimeFrom);
        }
        if (StringUtils.isNotEmpty(creationTimeTo)) {
            alLeastOneNotEmpty = true;
            sql.append("AND CREATION_TIME <= :CREATION_TIME_TO ");
            parameters.addValue("CREATION_TIME_TO", creationTimeTo);
        }

        if(!alLeastOneNotEmpty) {
            sql.delete(sql.indexOf("WHERE"), sql.length());
        }

        sql.append("ORDER BY CREATION_TIME DESC");

        final int totalCount = namedParameterJdbcTemplateForH2.queryForList(sql.toString(), parameters).size();

        sql.append(String.format(" LIMIT %d OFFSET %d", count, (page - 1) * count));

        log.info("sql:[{}],parameters:[{}]", sql.toString(), parameters.getValues());
        final List<Map<String, Object>> rows = namedParameterJdbcTemplateForH2.queryForList(sql.toString(), parameters);

        final List<TestCaseHistoryEntity> historyList = new ArrayList<TestCaseHistoryEntity>();
        for (int i = 0; i < rows.size(); i++) {
            final TestCaseHistoryEntity testCaseHistoryEntity = new TestCaseHistoryEntity();
            final Map<String, Object> row = rows.get(i);

            final String correlationId = (String) row.get("CORRELATION_ID");
            final String operatorUserToken = (String) row.get("OPERATOR_USER_TOKEN");
            final String soapCalls = (String) row.get("SOAP_CALLS");
            final String batchCalls = (String) row.get("BATCH_CALLS");
            final Long itemPrice = (Long) row.get("ITEM_PRICE");
            final Timestamp creationTime = (Timestamp) row.get("CREATION_TIME");

            testCaseHistoryEntity.setCorrelationId(correlationId);
            testCaseHistoryEntity.setOperatorUserToken(operatorUserToken);
            testCaseHistoryEntity.setSoapCalls(soapCalls);
            testCaseHistoryEntity.setBatchCalls(batchCalls);
            testCaseHistoryEntity.setItemPrice(itemPrice);
            testCaseHistoryEntity.setCreationTime(simpleDateFormat.format(creationTime));
            testCaseHistoryEntity.setTotalCount(totalCount);

            historyList.add(testCaseHistoryEntity);
        }
        return historyList;
    }

    public List<SoapRecordEntity> querySoapRecordList(final String correlationId) {
        final String sql = "SELECT * FROM SOAP_RECORD WHERE CORRELATION_ID = :CORRELATION_ID ORDER BY CREATION_TIME";

        final MapSqlParameterSource parameters = new MapSqlParameterSource()
                        .addValue("CORRELATION_ID", correlationId);

        log.info("sql:[{}],parameters:[{}]", sql, parameters.getValues());
        final List<Map<String, Object>> rows = namedParameterJdbcTemplateForH2.queryForList(sql, parameters);
        final List<SoapRecordEntity> soapRecordList = new ArrayList<SoapRecordEntity>();
        for (int i = 0; i < rows.size(); i++) {
            final SoapRecordEntity soapRecordEntity = new SoapRecordEntity();
            final Map<String, Object> row = rows.get(i);

            final String sendMessage = (String) row.get("SEND_MESSAGE");
            final String receiveMessage = (String) row.get("RECEIVE_MESSAGE");
            final String result = (String) row.get("RESULT");
            final String userMessage = (String) row.get("USER_MESSAGE");
            final String soapCall = (String) row.get("SOAP_CALL");
            final Timestamp creationTime = (Timestamp) row.get("CREATION_TIME");

            soapRecordEntity.setCorrelationId(correlationId);
            soapRecordEntity.setSendMessage(sendMessage);
            soapRecordEntity.setReceiveMessage(receiveMessage);
            soapRecordEntity.setResult(result);
            soapRecordEntity.setUserMessage(userMessage);
            soapRecordEntity.setSoapCall(soapCall);
            soapRecordEntity.setCreationTime(simpleDateFormat.format(creationTime));

            soapRecordList.add(soapRecordEntity);
        }
        return soapRecordList;
    }

    public List<RequestFileHistoryEntity> queryRequestFileList(final String out, final String correlationIdFrom,
        final String correlationIdTo, final Integer page, final Integer count) {
        final StringBuilder sql = new StringBuilder("SELECT DISTINCT t2.FILE_NAME,t1.CORRELATION_ID,t1.OPERATOR_USER_TOKEN,t1.BATCH_CALLS,t1.CREATION_TIME,t1.REQ_ID FROM TESTCASE_HISTORY t1,REQ_FILE_LOG t2 WHERE t1.REQ_ID is not null and t1.REQ_ID = t2.REQ_ID ");

        final MapSqlParameterSource parameters = new MapSqlParameterSource();

        if (StringUtils.isNotEmpty(out)) {
            sql.append("and t1.OPERATOR_USER_TOKEN = :OPERATOR_USER_TOKEN ");
            parameters.addValue("OPERATOR_USER_TOKEN", out);
        }
        if (StringUtils.isNotEmpty(correlationIdFrom)) {
            sql.append("and t1.CORRELATION_ID >= :CORRELATION_ID_FROM ");
            parameters.addValue("CORRELATION_ID_FROM", Long.valueOf(correlationIdFrom));
        }
        if (StringUtils.isNotEmpty(correlationIdTo)) {
            sql.append("and t1.CORRELATION_ID <= :CORRELATION_ID_TO ");
            parameters.addValue("CORRELATION_ID_TO", Long.valueOf(correlationIdTo));
        }
        sql.append("ORDER BY t1.CREATION_TIME DESC ");

        final int totalCount = namedParameterJdbcTemplateForH2.queryForList(sql.toString(), parameters).size();

        sql.append(String.format("LIMIT %d OFFSET %d", count.intValue(), (page - 1) * count.intValue()));

        log.info("sql:[{}],parameters:[{}]", sql.toString(), parameters.getValues());
        final List<Map<String, Object>> rows = namedParameterJdbcTemplateForH2.queryForList(sql.toString(), parameters);

        final List<RequestFileHistoryEntity> requestFileList = new ArrayList<RequestFileHistoryEntity>();
        for (int i = 0; i < rows.size(); i++) {
            final RequestFileHistoryEntity requestFileHistoryEntity = new RequestFileHistoryEntity();
            final Map<String, Object> row = rows.get(i);

            final String correlationId = (String) row.get("CORRELATION_ID");
            final String operatorUserToken = (String) row.get("OPERATOR_USER_TOKEN");
            final String batchCalls = (String) row.get("BATCH_CALLS");
            final String reqId = (String) row.get("REQ_ID");
            final String fileName = (String) row.get("FILE_NAME");
            final Timestamp creationTime = (Timestamp) row.get("CREATION_TIME");

            requestFileHistoryEntity.setCorrelationId(correlationId);
            requestFileHistoryEntity.setOperatorUserToken(operatorUserToken);
            requestFileHistoryEntity.setBatchCalls(batchCalls);
            requestFileHistoryEntity.setRequestFileId(reqId);
            requestFileHistoryEntity.setRequestFileName(fileName);
            queryRequestFileStatusAndIdFromNpp(fileName, requestFileHistoryEntity);
            requestFileHistoryEntity.setCreationTime(simpleDateFormat.format(creationTime));
            requestFileHistoryEntity.setTotalCount(totalCount);

            requestFileList.add(requestFileHistoryEntity);
        }
        return requestFileList;
    }

    public List<RequestFileDetailEntity> queryRequestFileDetail(final String fileDetailId) {
        final String sql = "select * from NPP_GDCB_REQ_DETAIL where REQ_LOG_ID = :REQ_LOG_ID";

        final MapSqlParameterSource parameters = new MapSqlParameterSource()
                        .addValue("REQ_LOG_ID", fileDetailId);

        log.info("sql:[{}],parameters:[{}]", sql, parameters.getValues());
        final List<Map<String, Object>> rows = namedParameterJdbcTemplateForDB2.queryForList(sql, parameters);

        final List<RequestFileDetailEntity> requestFileDetailList = new ArrayList<RequestFileDetailEntity>();
        for (int i = 0; i < rows.size(); i++) {
            final RequestFileDetailEntity requestFileDetailEntity = new RequestFileDetailEntity();
            final Map<String, Object> row = rows.get(i);

            final String reqType = (String) row.get("REQ_TYPE");
            final String correlationId = (String) row.get("CORRELATION_ID");
            final String gdcbRtnCode = (String) row.get("GDCB_RTN_CODE");

            requestFileDetailEntity.setFileDetailId(fileDetailId);
            requestFileDetailEntity.setCorrelationId(correlationId);
            requestFileDetailEntity.setRequestType(reqType);
            requestFileDetailEntity.setStatus(gdcbRtnCode);

            requestFileDetailList.add(requestFileDetailEntity);
        }
        return requestFileDetailList;
    }

    public void queryRequestFileStatusAndIdFromNpp(final String fileName,
        final RequestFileHistoryEntity requestFileHistoryEntity) {
        final String sql = "select case(status) when 'D' then 'SUCCESS' else 'PENDING' end as STATUS, ID from NPP_GDCB_REQ_LOG where FILE_NAME LIKE :FILE_NAME";

        final String queryFileName = "%"+fileName.substring(fileName.indexOf("request"));
        final MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("FILE_NAME", queryFileName);

        log.info("sql:[{}],parameters:[{}]", sql, parameters.getValues());
        final List<Map<String, Object>> rows = namedParameterJdbcTemplateForDB2.queryForList(sql, parameters);
        // only one or null
        if (rows.size() > 0) {
            final Map<String, Object> row = rows.get(0);
            final String status = (String) row.get("STATUS");
            final Long fileDetailId = (Long) row.get("ID");
            requestFileHistoryEntity.setStatus(status);
            requestFileHistoryEntity.setFileDetailId(String.valueOf(fileDetailId));
        }
    }

    public Map<String, Object> queryEnvConfig() {
        final String sql = "select * from app_env";

        log.info("sql:[{}]", sql);
        final List<Map<String, Object>> rows = jdbcTemplateForH2.queryForList(sql);

        final Map<String, Object> result = new HashMap<String, Object>();
        for (int i = 0 ; i < rows.size() ; i ++) {
                final Map<String, Object> row = rows.get(i);
                result.put((String) row.get("ENV_NAME"), (String) row.get("ENV_VALUE"));
        }

        return result;
    }

    public void saveEnvConfig(final Map<String, Object> config) {
        for (String key : config.keySet()) {
            final String updateSql = "update app_env set ENV_VALUE = :ENV_VALUE where ENV_NAME = :ENV_NAME";

            final MapSqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("ENV_VALUE", config.get(key))
                    .addValue("ENV_NAME", key);
            log.info("sql:[{}]", updateSql);
            final int updateCount = namedParameterJdbcTemplateForH2.update(updateSql, parameters);
            log.info("updateCount:{}", updateCount);
            if (updateCount == 0) {
                final String insertSql = "insert into app_env(ENV_VALUE,ENV_NAME) values(:ENV_VALUE,:ENV_NAME)";
                log.info("sql:[{}]", insertSql);
                namedParameterJdbcTemplateForH2.update(insertSql, parameters);
            }

        }
    }
    
    public List<Map<String, Object>> queryNoFileHistory() {
    	final String sql = new String("SELECT * FROM TESTCASE_HISTORY WHERE REQ_ID IS NULL AND BATCH_CALLS IS NOT NULL AND (TX_STATUS = 'A' OR TX_STATUS IS NULL)");
        log.info("sql:[{}]", sql);
        final List<Map<String, Object>> rows = jdbcTemplateForH2.queryForList(sql);
        
        return rows;
    }
    
    public void updateFileHistory(String reqId, Long id) {
    	final String sql = new String("UPDATE TESTCASE_HISTORY SET REQ_ID = :REQ_ID WHERE ID = :ID AND REQ_ID IS NULL AND BATCH_CALLS IS NOT NULL;");
        final MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("REQ_ID", reqId)
        		.addValue("ID", id);
        log.info("sql:[{}],parameters:[{}]", sql, parameters.getValues());
        namedParameterJdbcTemplateForH2.update(sql, parameters);
    }
    
    public void savelastBatchTime() {
            final String updateSql = "update app_env set ENV_VALUE = :ENV_VALUE where ENV_NAME = :ENV_NAME";
            final MapSqlParameterSource parameters = new MapSqlParameterSource()
                    .addValue("ENV_VALUE", simpleDateFormat.format(new Date()))
            		.addValue("ENV_NAME", "lastBatchTime");
            log.info("sql:[{}]", updateSql);
            final int updateCount = namedParameterJdbcTemplateForH2.update(updateSql, parameters);
            log.info("updateCount:{}", updateCount);
            if (updateCount == 0) {
                final String insertSql = "insert into app_env(ENV_VALUE,ENV_NAME) values(:ENV_VALUE, :ENV_NAME)";
                log.info("sql:[{}]", insertSql);
                namedParameterJdbcTemplateForH2.update(insertSql, parameters);
            }
    }

    public String getPaymentIdSeq() {
        final String sql = "SELECT PAYMENT_ID_SEQ.nextval as paymentIdSeq from dual";
        log.info("sql:[{}]", sql);
        return jdbcTemplateForH2.queryForObject(sql, String.class);
    }
}
