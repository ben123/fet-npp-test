package fet.npp.webService;

import java.io.StringWriter;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fet.ws.npp.Auth;
import com.fet.ws.npp.AuthResponse;
import com.fet.ws.npp.CarrierBilling;
import com.fet.ws.npp.Echo;
import com.fet.ws.npp.EchoResponse;
import com.fet.ws.npp.GetProvisioning;
import com.fet.ws.npp.GetProvisioningResponse;
import com.fet.ws.npp.SubscriptionRecurrence;
import com.fet.ws.npp.UserIdentifier;

import fet.npp.utils.DateUtils;

/**
 *
 * @author Karl, Tim
 *
 */
@Component
public class CarrierBillingClient {

    @Autowired
    CarrierBilling carrierBilling;
    public EchoResponse echo(final Echo echoParameters) {
    	return carrierBilling.echo(echoParameters);
    }
    
    public AuthResponse auth(final Auth authRequest) {
        return carrierBilling.auth(authRequest);
    }

    public GetProvisioningResponse getProvisioning(final GetProvisioning getProvisioningRequest) {
        return carrierBilling.getProvisioning(getProvisioningRequest);
    }

    public Echo getEchoRequest(final String correlationId, String echoUserMessage) {
    	final Echo echo = new Echo();
    	echo.setBillingAgreement("FET_TW");
    	echo.setCorrelationId(correlationId);
    	echo.setMessage(echoUserMessage);
    	echo.setVersion(3);
    	return echo;
    }
    
    public GetProvisioning getGetProvisioningRequest(final String correlationId, final String operatorUserToken) {
        final GetProvisioning getProvisioning = new GetProvisioning();
        getProvisioning.setCorrelationId(correlationId);
        getProvisioning.setVersion(3);
        final UserIdentifier userIdentifier = new UserIdentifier();
        userIdentifier.setOperatorUserToken(operatorUserToken);
        getProvisioning.setUserIdentifier(userIdentifier);
        getProvisioning.setUserLocale("zh-TW");
        getProvisioning.setBillingAgreement("FET_TW");
        return getProvisioning;
    }

    public Auth getAuthRequest(final String correlationId, final String operatorUserToken, final String total, final String dcbTosVersion, final String subscriptionId, final String subscriptionRecurrence, final Date date) {

        final Auth authRequest = new Auth();
        authRequest.setSubscriptionId(subscriptionId);
        authRequest.setSubscriptionRecurrenceFrequency(StringUtils.isNotEmpty(subscriptionRecurrence) ? SubscriptionRecurrence.fromValue(subscriptionRecurrence) : null);
        if (date != null) {
        	authRequest.setPurchaseTime(Long.valueOf(DateUtils.unixTimeFormat(date)));
        }
        authRequest.setOperatorUserToken(operatorUserToken);
        authRequest.setCorrelationId(correlationId);
        authRequest.setCurrency("TWD");
        authRequest.setDcbTosVersion(dcbTosVersion);
        authRequest.setItemPrice(Long.valueOf(total));
        authRequest.setTotal(Long.valueOf(total));
        authRequest.setUserLocale("zh-TW");
        authRequest.setVersion(3);
        authRequest.setBillingAgreement("FET_TW");
        authRequest.setPaymentDescription("FETnet Gbase test product");
        authRequest.setMerchantContact("http://www.fetnet.net/");
        return authRequest;
    }

    public String marshal(Object source, String localPart, Class<?>... type) {
        try {
            StringWriter stringWriter = new StringWriter();

            JAXBContext jaxbContext;

            jaxbContext = JAXBContext.newInstance(type);

            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // format the XML output
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            QName qName = new QName(null, localPart);
            JAXBElement<Object> root = new JAXBElement<Object>(qName, Object.class, source);

            jaxbMarshaller.marshal(root, stringWriter);

            String result = stringWriter.toString();
            return result;
        } catch (JAXBException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}
