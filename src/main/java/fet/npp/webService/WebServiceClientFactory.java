/**
 * Please modify this class to meet your needs This class is not complete
 */
package fet.npp.webService;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.fet.ws.npp.CarrierBilling;

/**
 *
 * @author Karl
 *
 */
@Component
public class WebServiceClientFactory {
	
    @Autowired
    private WSS4JInInterceptor loggingInInterceptor;
    @Autowired
    private WSS4JOutInterceptor loggingOutInterceptor;

    @Bean
    public CarrierBilling getCarrierBillingClient(@Value("${ws.npp.url}") String url) {
        final JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setAddress(url);
        factory.setServiceClass(CarrierBilling.class);
        
        factory.getInInterceptors().add(new LoggingInInterceptor());
        // decrypt
//        factory.getInInterceptors().add(loggingInInterceptor);
        
        factory.getOutInterceptors().add(new LoggingOutInterceptor());
        // sign
        factory.getOutInterceptors().add(loggingOutInterceptor);
        
        return factory.create(CarrierBilling.class);
    }

}
